package com.example.btapphc06;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;

public class MainActivity extends AppCompatActivity
{

    static final UUID mUUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TextView textView = findViewById(R.id.myText);

        BluetoothAdapter btAdapter = BluetoothAdapter.getDefaultAdapter();
        System.out.println(btAdapter.getBondedDevices());

        //I used a Bluetooth Scanner to find the MAC address. You will need to download the APP
        //to get the address.
        BluetoothDevice hc06 = btAdapter.getRemoteDevice("20:16:12:28:28:40");
        System.out.println(hc06.getName());

        BluetoothSocket btSocket = null;
        int counter = 0;
        assert btSocket != null;
        do {
            try {
                btSocket = hc06.createRfcommSocketToServiceRecord(mUUID);
                System.out.println(btSocket);
                btSocket.connect();
                System.out.println(btSocket.isConnected());
            } catch (IOException e) {
                e.printStackTrace();
            }
            counter++;
        } while (!btSocket.isConnected() && counter < 3);


        try {
            OutputStream outputStream = btSocket.getOutputStream();
            //byte[] text = {48,49,50,51};
            String text = "Ronald Bazillion";
            outputStream.write(text.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }

        InputStream inputStream = null;
        byte[] RcvBytes = new byte[20];
        try {
            inputStream = btSocket.getInputStream();
            inputStream.skip(inputStream.available());

            inputStream.read(RcvBytes, 0, RcvBytes.length);
            textView.setText(RcvBytes.toString());
//            for (int i = 0; i<26; i++) {
//
//                byte b = (byte) inputStream.read();
//                System.out.println((char) b);
//
//            }

        } catch (IOException e) {
            e.printStackTrace();
        }


        try {
            btSocket.close();
            System.out.println(btSocket.isConnected());
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
